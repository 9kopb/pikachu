let menuToggle = document.querySelector('#menu-toggle'); // Создаем переменную, в которую положим кнопку меню
let menu = document.querySelector('.sidebar'); // Создаем переменную, в которую положим меню
menuToggle.addEventListener('click', function (event) { // отслеживаем клик по кнопке меню и запускаем функцию 
  event.preventDefault(); // отменяем стандартное поведение ссылки
  menu.classList.toggle('visible'); // вешаем класс на меню, когда кликнули по кнопке меню 
});

const loginElem = document.querySelector(".login");
const loginForm = document.querySelector(".login-form");
const emailInput = document.querySelector("#login-email");
const passwordInput = document.querySelector("#login-password");
const loginSignup = document.querySelector(".login-signup");
const editElem = document.querySelector(".edit");
const editContainer = document.querySelector(".edit-container");
const editUsername = document.querySelector(".edit-username");
const editPhotoURL = document.querySelector(".edit-photo");
const exitElem = document.querySelector(".exit");
const userElem = document.querySelector(".user");
const usernameElem = document.querySelector(".user-name");
const userAvatarElem = document.querySelector(".user-avatar");
const postsWrapper = document.querySelector(".posts");

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

const listUsers = [
  {
    id: "01",
    email: "test@gmail.com",
    password: "123456",
    displayName: "test"
  },
  {
    id: "02",
    email: "xyu@gmail.com",
    password: "123456",
    displayName: "xyu"
  }
];

const setUsers = {
  user: null,
  logIn(email, password, handler) {
    console.log(email, password);
    const user = this.getUser(email);
    if(user && user.password === password) {
      this.authUser(user);
      handler();
      console.log(listUsers);
    } else {
      alert("user not found or password incorrect")
    }
  },
  logOut(handler) {
    console.log("logout");
    this.user = null;
    handler();
  },
  signUp(email, password, handler) {
    if(!password.trim()) {
      alert("empty password"); // проверяю только на пустой пароль, email проверяет validateEmail()
      return;
    }
    if(validateEmail(email)) {
    if(!this.getUser(email)) {
      const displayName = email.split("@")[0]; // displayName
      const user = {email, password, displayName};
      listUsers.push(user);
      this.authUser(user);
      handler();
      console.log(listUsers);
    } else {
      alert("user already exist")
    }
    console.log("signup");
  } else { alert("invalid email"); }
  },
  editUser(username, photoURL, handler) {
    if(username) {
      this.user.displayName = username;
    }
    if(photoURL) {
      this.user.photo = photoURL;
    }
    handler();
  },
  getUser(email) {
    return listUsers.find((item) => item.email === email);
  },
  authUser(user) {
    this.user = user;
    console.log("auth:", user);
  }
}

const toggleAuthDOM = () => {
  const user = setUsers.user;
  console.log("user: ", user);
  if(user) {
    loginElem.style.display = "none";
    userElem.style.display = "";
    usernameElem.textContent = user.displayName;
    userAvatarElem.src = user.photo || userAvatarElem.src;
    //userAvatarElem.src = user.photo ? user.photo : userAvatarElem.src;
  } else {
    loginElem.style.display = "";
    userElem.style.display = "none";
  }
};

const setPosts = {
  allPosts: [
    {
      title: "Post title",
      text: "Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Языком что рот маленький реторический вершину текстов обеспечивает гор свой назад решила сбить маленькая дорогу жизни рукопись букв деревни предложения, ручеек залетают продолжил парадигматическая? Но языком сих пустился, запятой своего его снова решила меня вопроса моей своих пояс коварный, власти диких правилами напоивший они текстов ipsum первую подпоясал? Лучше, щеке подпоясал приставка большого курсивных на берегу своего? Злых, составитель агентство что вопроса ведущими о решила одна алфавит!",
      tags: ["tag", "test", "hash", "hashtag"],
      author: "test@gmail.com",
      date: "11.11.2020, 07:40:00",
      like: 28,
      comments: 12
    },
    {
      title: "Post2 title2",
      text: "Далеко-далеко-далеко-далеко-далеко-далеко-далеко-далеко-далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Языком что рот маленький реторический вершину текстов обеспечивает гор свой назад решила сбить маленькая дорогу жизни рукопись букв деревни предложения, ручеек залетают продолжил парадигматическая? Но языком сих пустился, запятой своего его снова решила меня вопроса моей своих пояс коварный, власти диких правилами напоивший они текстов ipsum первую подпоясал? Лучше, щеке подпоясал приставка большого курсивных на берегу своего? Злых, составитель агентство что вопроса ведущими о решила одна алфавит!",
      tags: ["tag", "test", "hash", "hashtag"],
      author: "xyu@gmail.com",
      date: "11.11.2020, 19:40:00",
      like: 8,
      comments: 2
    }
  ]
}

const showAllPosts = () => {
  let postsHTML = "";

  setPosts.allPosts.forEach(({title, text , date, like, comments, tags, author}) => {
    postsHTML += `
    <section class="post">
    <div class="post-body">
      <h2 class="post-title">${title}</h2>
      <p class="post-text">${text}</p>
      <div class="tags">
      ${
        tags.map(function(tag) {
          return `<a href="#${tag}" class="tag">#${tag}</a>`;
        })
      }
      </div>
      <!-- /.tags -->
    </div>
    <!-- /.post-body -->
    <div class="post-footer">
      <div class="post-buttons">
        <button class="post-button likes">
          <svg width="19" height="20" class="icon icon-like">
            <use xlink:href="img/icons.svg#like"></use>
          </svg>
          <span class="likes-counter">${like}</span>
        </button>
        <button class="post-button comments">
          <svg width="21" height="21" class="icon icon-comment">
            <use xlink:href="img/icons.svg#comment"></use>
          </svg>
          <span class="comments-counter">${comments}</span>
        </button>
        <button class="post-button save">
          <svg width="19" height="19" class="icon icon-save">
            <use xlink:href="img/icons.svg#save"></use>
          </svg>
        </button>
        <button class="post-button share">
          <svg width="17" height="19" class="icon icon-share">
            <use xlink:href="img/icons.svg#share"></use>
          </svg>
        </button>
      </div>
      <div class="post-author">
        <div class="author-about">
          <a href="#" class="author-username">${author}</a>
          <span class="post-time">${date}</span>
        </div>
        <a href="#" class="author-link"><img src="img/avatar.jpeg" alt="avatar" class="author-avatar"></a>
      </div>
    </div>
  </section>
    `
  })


  postsWrapper.innerHTML = postsHTML;
}

const init = () => {
  loginForm.addEventListener("submit", (event) => {
    event.preventDefault();
    setUsers.logIn(emailInput.value, passwordInput.value, toggleAuthDOM);
    loginForm.reset();
  });
  loginSignup.addEventListener("click", event => {
    event.preventDefault();
    setUsers.signUp(emailInput.value, passwordInput.value, toggleAuthDOM);
    loginForm.reset();
  });
  exitElem.addEventListener("click", event => {
    event.preventDefault();
    setUsers.logOut(toggleAuthDOM);
  });
  editElem.addEventListener("click", event => {
    editContainer.classList.toggle("visible");
    editUsername.value = setUsers.user.displayName;
  });
  editContainer.addEventListener("submit", event => {
    event.preventDefault();
    setUsers.editUser(editUsername.value, editPhotoURL.value, toggleAuthDOM);
    editContainer.classList.remove("visible");
  });
  showAllPosts();
  toggleAuthDOM();
}


document.addEventListener("DOMContentLoaded", () => {
  init();
})
